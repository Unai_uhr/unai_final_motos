const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???

//var multer = require('multer');
//const upload = multer({dest:'/assests'}).single("foto");

const app = express();
const baseUrl = '/miapi';

app.use(cors());


//La configuració de la meva bbdd
ddbbConfig = {
   user: 'unai-hernandez-7e3_motos_data_base',
   host: 'postgresql-unai-hernandez-7e3.alwaysdata.net',
   database: 'unai-hernandez-7e3_motos_data_base',
   password: 'unhero276R',
   port: 5432
};

/*ddbbConfig = {
    user: 'root',
    host: 'localhost',
    database: 'unai-hernandez-7e3_motos_data_base',
    password: '',
    port: 3000
 };*/

//El pool es un congunt de conexions
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
const getMotos = (request, response) => {
   var consulta = "SELECT * FROM  motos"

   console.log("hola")
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}

const getMotoById = (request, response) => {

  console.log(request.params.id)
   var consulta = `SELECT * FROM  motos WHERE id= ${request.params.id}`; 
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
 
    const getMotoByMarca= (request, response) => {

            const marca = request.query.marca
            var consulta = `SELECT * FROM  motos where marca = '${marca}' `
            pool.query(consulta, (error, results) => {
                if (error) {
            throw error
        }
        response.status(200).json(results.rows)
        console.log(results.rows);    
        }); 
}


  const deleteMotoById = (request, response) => {

    console.log(request.params.id)
     var consulta = `DELETE FROM  motos WHERE id= ${request.params.id}`; 
     pool.query(consulta, (error, results) => {
         if (error) {
             throw error
         }

         response.status(200).json({'mensaje':"Moto eliminada de la base de datos"});
         console.log(results.rows);
     });
  }

  const insertMoto = (request, response) => {


    var consulta = `INSERT INTO motos (marca,modelo,year,foto,precio) VALUES ('${request.query.marca}', '${request.query.modelo}','${request.query.year}','${request.query.foto}','${request.query.precio}')`

         pool.query(consulta, (error, results) => {
             if (error) {
                 throw error
             }
             //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
             response.status(200).json({'mensaje':"Moto insertada"})
             console.log(results.rows);
         });
      }


 



app.get(baseUrl + '/getmotos', getMotos);
app.get(baseUrl + '/getmoto/:id', getMotoById);
app.get(baseUrl + '/getmotosbymarca', getMotoByMarca);

app.delete(baseUrl + '/deletemoto/:id', deleteMotoById )

app.post(baseUrl +'/insertmoto', insertMoto)


//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
