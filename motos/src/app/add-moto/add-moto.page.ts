import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataMotosService } from '../services/data-motos.service';
import { AngularFireStorage ,AngularFireUploadTask} from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize, ignoreElements, last, switchMap } from 'rxjs/operators'

@Component({
  selector: 'app-add-moto',
  templateUrl: './add-moto.page.html',
  styleUrls: ['./add-moto.page.scss'],
})
export class AddMotoPage implements OnInit {

  image;
  marca:string;
  modelo:string;
  precio:string;
  year:string;

  uploadPercent :Observable<number>;
  downloadURL;
  urlImage : any;

  inputError=false;
  


  constructor(
    private motoData: DataMotosService, 
    private router: Router,
    private route: ActivatedRoute,
    private fireStorage: AngularFireStorage,
  ) { }

  ngOnInit() {
  }

  cargarFoto(event){
    console.log("FOTO", event.target.files[0])

    const id = Math.random().toString(36).substring(2);
    const file =  event.target.files[0];
    const filePath = `motosApiUnai_${id}`;
    const ref = this.fireStorage.ref(filePath);
    const task= this.fireStorage.upload(filePath,file)

    this.uploadPercent = task.percentageChanges();
    this.urlImage = ref.getDownloadURL()

    task.snapshotChanges().pipe(
      last(),  
      switchMap(() => ref.getDownloadURL())
    ).subscribe(url => this.urlImage=url);
    console.log("URL ", this.urlImage)

  }


  async guardarMoto(){

      if(this.marca=="" || this.modelo=="" ||this.year=="" ||this.urlImage=="" ||this.precio=="" ){
        this.inputError=true;
      }else{
        this.inputError=false;
      

        let motoNueva={
          marca:this.marca,
          modelo:this.modelo,
          year:this.year,
          foto:this.urlImage,
          precio:this.precio
        }



       await this.motoData.insertMoto(motoNueva);

      }

      this.router.navigate(['home']);
  }

}
