import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';
import { MotoItem } from '../interfaces/interfaces';

@Component({
  selector: 'app-detalle-moto',
  templateUrl: './detalle-moto.page.html',
  styleUrls: ['./detalle-moto.page.scss'],
})
export class DetalleMotoPage implements OnInit {

  motoDetalle:MotoItem;

  constructor(private route: ActivatedRoute, private router: Router) {

    this.route.queryParams.subscribe(params => {
      if(this.router.getCurrentNavigation().extras.state){
        this.motoDetalle = this.router.getCurrentNavigation().extras.state.motoItem;
       // console.table(this.motoDetalle) 
      }
    });
   }

   borrarMoto(){

    const urlBase= "http://unai-hernandez-7e3.alwaysdata.net"
    const url = `${urlBase}/miapi/deletemoto/${this.motoDetalle.id}`;
    fetch(url, {
      "method": "DELETE"
    })
    .then(response => {
        this.router.navigateByUrl('/home');
      });
  

   }
   

  ngOnInit() { }

}
