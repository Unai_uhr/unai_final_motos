import { Component } from '@angular/core';
import { DataMotosService } from '../services/data-motos.service';
import { FormsModule } from '@angular/forms';
import { MotoItem } from '../interfaces/interfaces';
import { NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    listaMotos :MotoItem[]=[];
    listaMotosPorMarca: any;
    marca:string='todas';


    menuList=[
      { 'marca': 'Ducati',
        'logo' : 'https://cdn-0.motorcycle-logos.com/wp-content/uploads/2016/10/Ducati-Logo.png',
        'value': 'Ducati'
      },
      { 'marca': 'Yamaha',
        'logo' : 'https://i.pinimg.com/originals/35/18/48/35184802e7fd8aaad3a84bef34ea37c6.jpg',
        'value': 'Yamaha'
      },
      { 'marca': 'Honda',
        'logo' : 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Honda_Logo.svg/1200px-Honda_Logo.svg.png',
        'value': 'Honda'
      },
      { 'marca': 'Todas las marcas',
        'logo' : 'https://www.pasionbiker.com/wp-content/uploads/2016/07/marcas-mejores-motos-del-mundo.jpg',
        'value': 'todas'
      },
    
    ]


    

  constructor(
    private motoData: DataMotosService, 
    private router: Router,
    private menu: MenuController,
    //private route?: ActivatedRoute
    
    ) {

    

    }

  ngOnInit():void{
    console.log('ONINIT')
    this.cargarListaMotos();
      
  
  }

  toggleMenu(){
    this.menu.toggle();
  }

  ionViewWillEnter() {
     
    this.cargarListaMotos();

  }

  async cargarListaMotos(){
    this.listaMotos=await this.motoData.getMotos();
  }


  async cargarListaMotosFiltrada(marca:string){
    console.log("filtrar")
    this.marca=marca; 
    this.listaMotos=await this.motoData.getMotosFiltro(this.marca);
    
    console.table(this.listaMotos);
  }

  filtrarMarca(marca:string){
    if(marca!='todas'){
     this.cargarListaMotosFiltrada(marca);
    }else{
     this.cargarListaMotos();
    }
   
}
  

  abritDetalle(moto:MotoItem){
    console.log('click',moto)
    let navigationExtras: NavigationExtras = {
      state: {
        motoItem: moto,
      }
    };
    this.router.navigate(['detalle-moto'], navigationExtras);
  }

  addMotoNueva(){
    this.router.navigate(['app-add-moto']);
  }
  

}
