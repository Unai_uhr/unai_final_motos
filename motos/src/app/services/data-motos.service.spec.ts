import { TestBed } from '@angular/core/testing';

import { DataMotosService } from './data-motos.service';

describe('DataMotosService', () => {
  let service: DataMotosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataMotosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
