import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


//const urlBase= "http://motos.puigverd.org"
const urlBase= "http://unai-hernandez-7e3.alwaysdata.net/miapi/"
const urlBaseLocal= "http://localhost:3000"

@Injectable({
  providedIn: 'root'
})
export class DataMotosService {

  listaMotos: any[]=[]

  constructor( private http: HttpClient) { }


  async getMotos(){
    //const respuesta = await fetch(`${urlBase}/motos`);
    const respuesta = await fetch(`${urlBase}getmotos`);
    this.listaMotos = await respuesta.json();
    //console.log('DATA',this.listaMotos);
    return this.listaMotos;
  }

  async getMotosFiltro(marca:string){
    console.log('DATA',marca);
    const respuesta = await fetch(`http://unai-hernandez-7e3.alwaysdata.net/miapi/getmotosbymarca?marca=${marca}`);
    

    this.listaMotos = await respuesta.json();
    //console.log('DATA',this.listaMotos);
    return this.listaMotos;
  }

  async insertMoto(data:any){

    console.log("DATA",data.marca)

     let url=`http://unai-hernandez-7e3.alwaysdata.net/miapi/insertmoto?marca=${data.marca}&modelo=${data.modelo}&year=${data.year}&foto=${data.foto}&precio=${data.precio}`;
   

     fetch(url, {
     method: "POST",
     
   })

    

    


  }
  


}
